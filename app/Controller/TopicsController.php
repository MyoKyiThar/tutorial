<?php
class TopicsController extends AppController {
   
    public $helpers = array('Html', 'Form');   

    public $components = array('Flash');

    public function index() {
        $this->set('topics', $this->Topic->find('all'));
        $this->set('title_for_layout', 'Latest Topics for all');
        $this->paginate = array(
            'limit' => 5,
            'order' => array('Topic.title' => 'asc' )
        );
        $topics = $this->paginate('Topic');
        $this->set(compact('topics'));
    }

    public function view($id = null) { 
        $this->Topic->id = $id;
        $this->set('topic', $this->Topic->read());         
    }

    public function add() {
       if ($this->request->is('post')) {  
          $this->request->data['Topic']['user_id'] = $this->Auth->user('id'); 
           if(!empty($this->data)) {
               if(!empty($this->data['Topic']['image_field_name']['name'])) {
                   $file = $this->data['Topic']['image_field_name']; //put the  data into a var for easy use
                   $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                   $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions
                   if(in_array($ext, $arr_ext)) {                        
                       if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload_folder' . DS . $file['name'])) {                           
                          $this->request->data['Topic']['image_field_name'] = '/img/upload_folder/'.$file['name'];                
                       } else {
                          $this->Flash->error(__('The data could not be saved. Please, try again.'), 'default',array('class'=>'errors'));
                       }
                   }
               } else {
                  $this->request->data['Topic']['image_field_name'] = '';
            }                   
           if ($this->Topic->save($this->request->data)) {
               $this->Flash->success('New topic updated');
               $this->redirect(array('action' => 'index'));
               } else {
                   $this->Flash->error('Failed!');
                   $this->redirect(array('action' => 'index'));
                }
           }
       }
    }

   public function edit($id = null) {
       
       if (!$id) {
       }

       $topic = $this->Topic->findById($id);
       if (!$topic) {
       }

       if(!empty($this->data)) {
               if(!empty($this->data['Topic']['image_field_name']['name'])) {
                   $file = $this->data['Topic']['image_field_name']; //put the  data into a var for easy use
                   $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                   $arr_ext = array('jpg', 'jpeg', 'gif', 'png'); //set allowed extensions
                   if(in_array($ext, $arr_ext)) {        
                       if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload_folder' . DS . $file['name'])) {                           
                          $this->request->data['Topic']['image_field_name'] = '/img/upload_folder/'.$file['name'];                
                       } else {
                          $this->Session->setFlash(__('The data could not be saved. Please, try again.'), 'default',array('class'=>'errors'));
                       }
                   }
               } else {
                       $this->request->data['Topic']['image_field_name'] = $this->$topic['Topic']['image_field_name'] ;
                   }

       if ($this->request->is(array('post', 'put'))) {
           $this->Topic->id = $id;         
               if ($this->Topic->save($this->request->data)) {
                   $this->Flash->success(__('Your post has been updated.'));
                   $this->redirect(array('action' => 'index'));
               }
           }   
           
       }
           if (!$this->request->data) {
               $this->request->data = $topic;
           }
   
   }

    public function delete($id) {
        if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Topic->delete($id)) {
            $this->Flash->success(__('The topic with id: %s has been deleted.', h($id)));
        }else {
            $this->Flash->error(__('The topic with id: %s could not be deleted.', h($id)));
        }
        $this->redirect(array('action' => 'index'));
    } 

     public function logout(){
     $this->Session->delete("User");   
        $this->Session->destroy();        
        $this->redirect($this->Auth->logout());
    }
}