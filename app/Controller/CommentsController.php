<?php
class CommentsController extends AppController { 

    public $helpers = array('Html', 'Form');
    public $components=array('Flash');
    var $uses=array('Topic','Comment');

    public function add() {
        if ($this->request->is('post')) {       
        if(!empty($this->data)) {
                if(!empty($this->data['Comment']['image_field_name']['name'])) {
                    $file = $this->data['Comment']['image_field_name']; //put the  data into a var for easy use
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'gif'); //set allowed extensions
                    if(in_array($ext, $arr_ext)) {
                        //do the actual uploading of the file. First arg is the tmp name, second arg is
                        //where we are putting it
                        if(move_uploaded_file($file['tmp_name'], WWW_ROOT . 'img/upload_folder' . DS . $file['name'])) {
                            //prepare the filename for database entry
                            $this->request->data['Comment']['image_field_name'] = '/img/upload_folder/'.$file['name'];                
                        } else {
                            $this->Session->setFlash(__('The data could not be saved. Please, try again.'), 'default',array('class'=>'errors'));
                        }
                    }
                }
                 else {
                    $this->request->data['Comment']['image_field_name'] = '';
                }
                
                $this->request->data['Comment']['user_id'] = $this->Auth->user('id');
                $this->request->data['Comment']['username'] = $this->Auth->user('username');
                if(!empty($this->data['Comment']['image_field_name']['name']) || !empty($this->data['Comment']['body'])) {        
                    if ($this->Comment->save($this->request->data)) {
                        $this->Flash->success(__('Save comment'));
                                // pr($this->request->data);
                       $this->redirect(array('controller'=>'topics', 'action'=>'view', $this->data['Comment']['topic_id']));
                    }
                }else {
                    //$this->Session->setFlash('Failed!');
                    $this->Flash->error(__('Unable your comment'));
                    $this->redirect(array('controller'=>'topics', 'action'=>'view', $this->data['Comment']['topic_id']));
                }
               
            }
        }

    }   

    public function editComment($id = null, $topic_id = null,$user_id = null) {
        $this->Comment->id = $id;
        $this->Topic->id = $topic_id;
        if($this->request->is('get')) {
            $this->request->data=$this->Comment->read();
        }else if($user_id == $this->Auth->user('id')) {           
            if($this->Comment->save($this->request->data)) {
                $this->Flash->success('Your comment has been updated');
                $this->redirect(array('controller'=>'topics','action' => 'view/'.$topic_id));
            }
        }else {       
            $this->Flash->success(__('Unable Comment')); 
            $this->redirect(array('controller'=>'topics','action' => 'view/'.$topic_id));           
        }
    }

    public function deleteComment($id = null, $topic_id = null) {
         if ($this->request->is('get')) {
            throw new MethodNotAllowedException();
        }     
        if($this->Comment->delete($id)){
            return $this->redirect(array('controller'=>'topics', 'action' => 'view', $topic_id));
        }
    }

    public function view() {
        $this->set('comments', $this->Comment->find('all'));
        $this->set('title_for_layout', 'Latest Topics for all');
        $this->paginate = array(
            'limit' => 5,
            'order' => array('Comment.body' => 'asc' )
        );
        $topics = $this->paginate('Comment');
        $this->set(compact('commentsset_flash'));
    }

    public function logout(){           
        $this->redirect($this->Auth->logout());
    }
    
   
} 