<?php

class UsersController extends AppController {
    public $paginate = array(
        'limit' => 25,
        'conditions' => array('status' => '1'),
        'order' => array('User.username' => 'asc' ) 
    );

    public function beforeFilter() {
      parent::beforeFilter();
    }

    public function register() {
      if ($this->request->is('post')) {
          $this->User->create();
              if ($this->User->save($this->request->data)) {
                    $this->Flash->success(__('New user registered'));
                        $this->redirect(array('action' => 'login'));
                    }    
                    $this->Flash->error(__('Could not register user'));
        }
    }

    public function login() { 
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect(array('controller'=>'Topics', 'action'=>'index'));
            }
            $this->Flash->error(__('Incorrect username or password'));
        }
    }
    
    public function logout() {
        $this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }
    
    
} 