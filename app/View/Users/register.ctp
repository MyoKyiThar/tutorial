<div class="users form">

<?php echo $this->Form->create('User');?>
   <fieldset>
       <legend><?php echo __('Add User'); ?></legend>
       <?php echo $this->Form->input('username');
       echo $this->Form->input('email');
       echo $this->Form->input('password');
       echo $this->Form->input('password_confirm', array('label' => 'Confirm Password *', 'maxLength' => 255, 'title' => 'Confirm password', 'type'=>'password'));
       echo $this->Form->input('gender', array(
           'options' => array( 'Male' => 'Male', 'Female' => 'Female')
       ));
       echo $this->Form->input('address');
        
       echo $this->Form->submit('Add User', array('controller' => 'users', 'action' => 'register')); 
?>
   </fieldset>
<?php echo $this->Form->end(); ?>
</div>
<?php 
echo $this->Html->link( "Back to Login",   array('action'=>'login') ); 
?>