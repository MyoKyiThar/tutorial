<div class="users form">
	<?php echo $this->Form->create('User'); ?>
	    <fieldset>
	        <legend><?php echo __('User Registeration'); ?></legend>
	        <?php echo $this->Form->input('username');
	        echo $this->Form->input('password');
	        echo $this->Form->input('role', array(
	            'options' => array('admin' => 'Admin', 'author' => 'Author')
	        ));
	    ?>
	    </fieldset>
	<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php
 echo $this->Html->link( "Back to Login",   array('action'=>'login') ); 
?>
