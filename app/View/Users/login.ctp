<!-- <h2><?php echo __('Login'); ?></h2> -->

<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('User'); ?>
<?php

	echo $this->Form->inputs(array(
			'username',
			'password',
			'legend' => __('Please login here!')
		));

	echo $this->Form->end('Sign In');
	echo $this->Html->link('Register for new user', array('controller' => 'users', 'action' => 'register'));
?>
