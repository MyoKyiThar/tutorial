<table>
    <thead>
         <tr>
            <td>
                 <?php echo $this->Html->link('Add Topic', array('action'=>'add')); ?>
            </td>
           <td>
               <td>
                    <td><?php echo $this->Html->link('Logout', array('action'=>'logout')); ?></td>
               </td>
           </td>
        </tr>

    </thead>
    <tbody>
        <tr>
            <th>Title</th>
            <th>Created</th>  
            <th>Actions</th>                      
        </tr>

        <?php foreach ($topics as $topic) : ?>
           
        <tr>
                      
            <td>
                <?php echo $this->Html->link($topic['Topic']['title'],
                array('controller' => 'topics', 'action' => 'view', $topic['Topic']['id'])); ?>
                by
                <?php echo h($topic['User']['username']); ?>
            </td>
            <td><?php echo $topic['Topic']['created']; ?></td>
            <td>

                <?php
                    if ($topic['Topic']['user_id'] == $this->Session->read('Auth.admins.id')) {
                        echo $this->Html->link('Edit', array('action'=>'edit',$topic['Topic']['id'])); 
                        echo "\n";
                        echo $this->Form->postLink('Delete', array('action' => 'delete', $topic['Topic']['id']), array('confirm' => 'Are you sure?')); 
                    }        
                ?> 
            </td> 

        </tr>
    <?php endforeach; ?>
    <?php unset($post); ?>
    </tbody>
</table>
<?php
    $paginator = $this->Paginator;
    echo "<div class='paging'>";      
       if($paginator->hasPrev()){
           echo $paginator->prev("Prev");
       }        
       
       echo $paginator->numbers(array('modulus' => 2));       
      
       if($paginator->hasNext()){
           echo $paginator->next("Next");
       }    
    echo "</div>";
?>