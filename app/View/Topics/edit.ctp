<h1>Edit Post</h1>
<?php
	echo $this->Form->create('Topic',array('enctype' => 'multipart/form-data' ,'url' => array('controller' => 'topics', 'action' => 'edit')));
	echo $this->Form->input('title');
	echo $this->Form->input('body', array('rows' => '3'));
?>
<?php
if(!empty($this->data['Topic']['image_field_name'])): ?>
   <div class="input">
    <label>Current Image:</label>
    <?php 
      echo $this->Html->image($this->data['Topic']['image_field_name'], array('width'=>100));
    ?>
   </div>
 <?php endif; ?>
 <?php
	echo $this->Form->input("image_field_name",array("type"=>"file"));
	echo $this->Form->end('Save Topic');	
?>

